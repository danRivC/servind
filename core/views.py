from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from solutions.models import PrincipalSolution
from projects.models import Project
from about_us.models import TestimonialModel
from contact.forms import QuickContact


class HomeView(FormView):
    template_name = 'core/home.html'
    form_class = QuickContact
    success_url = 'home'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['solutions'] = PrincipalSolution.objects.all()
        context['projects'] = Project.objects.all()
        context['testimonials'] = TestimonialModel.objects.all()
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('home')+'?ok'





