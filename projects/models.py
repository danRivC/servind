from django.db import models

class Project(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descripción')
    category = models.ManyToManyField('projects.CategoryProject', verbose_name='Categoria')
    image = models.ImageField(upload_to='projects/', verbose_name='Imagen')
    starting_date = models.DateTimeField(verbose_name='Fecha de inicio')
    finish_date = models.DateTimeField(null=True, blank= True, verbose_name='Fecha de culminación')

    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyectos'
        ordering = ['-created']

    def __str__(self):
        return self.title

class CategoryProject(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo descripcion')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        ordering = ['-created']

    def __str__(self):
        return self.title

