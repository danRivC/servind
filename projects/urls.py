from django.urls import path
from .views import *
urlpatterns=([
    path('project/<int:pk>/<slug:slug>/', ProjectDetailView.as_view(), name='project_detail'),
    path('', ProjectsViews.as_view(), name='projects'),
])