# Generated by Django 2.1.4 on 2019-01-21 03:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='categoryproject',
            options={'ordering': ['-created'], 'verbose_name': 'Categoria', 'verbose_name_plural': 'Categorias'},
        ),
    ]
