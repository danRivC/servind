# Generated by Django 2.1.4 on 2019-01-19 03:04

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryProject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Titulo descripcion')),
                ('created', models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')),
                ('updated', models.DateField(auto_now=True, verbose_name='Fecha de modificación')),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Titulo')),
                ('description', models.TextField(verbose_name='Descripción')),
                ('image', models.ImageField(upload_to='projects/', verbose_name='Imagen')),
                ('starting_date', models.DateTimeField(verbose_name='Fecha de inicio')),
                ('finish_date', models.DateTimeField(blank=True, null=True, verbose_name='Fecha de culminación')),
                ('created', models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')),
                ('updated', models.DateField(auto_now=True, verbose_name='Fecha de modificación')),
                ('category', models.ManyToManyField(to='projects.CategoryProject', verbose_name='Categoria')),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
                'ordering': ['-created'],
            },
        ),
    ]
