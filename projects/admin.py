from django.contrib import admin
from .models import *


class ProjectAdmin(admin.ModelAdmin):
    readonly_fields = ['created','updated']


class CategoryProjectAdmin(admin.ModelAdmin):
    readonly_fields = ['created','updated']


admin.site.register(Project, ProjectAdmin)
admin.site.register(CategoryProject, CategoryProjectAdmin)
