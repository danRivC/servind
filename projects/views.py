from django.shortcuts import render
from django.views.generic import DetailView, ListView
from .models import *

class ProjectDetailView(DetailView):
    model = Project
    template_name = 'projects/project_detail.html'


class ProjectsViews(ListView):
    model = Project
    template_name = 'projects/projects.html'

