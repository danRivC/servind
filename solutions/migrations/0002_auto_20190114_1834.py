# Generated by Django 2.0.2 on 2019-01-14 23:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('solutions', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='principalsolution',
            name='secondSolution',
        ),
        migrations.AddField(
            model_name='principalsolution',
            name='secondSolution',
            field=models.ManyToManyField(to='solutions.SecondSolution', verbose_name='Solucion Complementaria'),
        ),
    ]
