from django.db import models


class SecondSolution(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descripción')
    image = models.ImageField(upload_to='secondSolution', verbose_name='Imagen')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Solución Complementaria'
        verbose_name_plural = 'Soluciones Complementarias'
        ordering=['-created']

    def __str__(self):
        return self.title


class PrincipalSolution(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descripción')
    image = models.ImageField(upload_to='principalSolution', verbose_name='Imagen')
    secondSolution = models.ManyToManyField('solutions.SecondSolution', verbose_name='Solucion Complementaria')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Solución Principal'
        verbose_name_plural = 'Soluciones Principales'
        ordering=['-created']

    def __str__(self):
        return self.title

