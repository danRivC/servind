from django.urls import path
from .views import SolutionsViews, ServicesViews
urlpatterns=([
    path('<int:pk>/<slug:slug>/', SolutionsViews.as_view(), name='solutions_detail'),
    path('', ServicesViews.as_view(), name='solutions'),
])