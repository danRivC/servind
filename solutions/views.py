from django.shortcuts import render
from .models import PrincipalSolution, SecondSolution
from django.views.generic import DetailView, ListView
from about_us.models import Client

class SolutionsViews(DetailView):
   model = PrincipalSolution
   template_name = 'solutions/solutions_detail.html'
   def get_context_data(self, *, object_list=None, **kwargs):
      context = super().get_context_data(**kwargs)

      context['solutions'] = PrincipalSolution.objects.all()
      return context


class ServicesViews(ListView):
   model = PrincipalSolution
   template_name = 'solutions/solutions.html'
   def get_context_data(self, *, object_list=None, **kwargs):
      context = super().get_context_data(**kwargs)
      context['clients'] = Client.objects.all()
      context['solutions'] = PrincipalSolution.objects.all()
      return context




