from  django import forms
from .models import ContactUserModel, QuickContactModel


class contactForm(forms.ModelForm):

    class Meta:
        model = ContactUserModel
        fields = ['name', 'email', 'phone_number', 'subject', 'message']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Número de teléfono'}),
            'subject': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Asunto'}),
            'message': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Mensaje'}),

        }
        labels = {
            'name': '', 'email': '', 'phone_number': '', 'subject': '', 'message': ''
        }


class QuickContact(forms.ModelForm):
    class Meta:
        model = QuickContactModel
        fields = ['name', 'email', 'phone_number']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Tu Nombre'}),
            'email': forms.TextInput(attrs={'placeholder': 'Email'}),
            'phone_number': forms.TextInput(attrs={'placeholder': 'Número telefónico'}),
        }
        labels = {
            'name':'', 'email': '', 'phone_number': ''
        }