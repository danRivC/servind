from django.shortcuts import render
from django import forms
from .forms import  contactForm
from django.views.generic.edit import FormView
from django.urls import reverse_lazy
from solutions.models import PrincipalSolution

class ContactForm(FormView):
    template_name = 'contact/contact.html'
    form_class = contactForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['solutions']= PrincipalSolution.objects.all()
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('contact')+'?ok'



