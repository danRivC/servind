from django.contrib import admin
from .models import ContactUserModel, QuickContactModel


class ContactAdmin(admin.ModelAdmin):
    readonly_fields = ['name', 'email','phone_number', 'subject', 'message','created','updated']


class QuickContactAdmin(admin.ModelAdmin):
    readonly_fields = ['name', 'email', 'phone_number', 'created', 'updated']


admin.site.register(QuickContactModel, QuickContactAdmin)
admin.site.register(ContactUserModel, ContactAdmin)


