from django.db import models

class ContactUserModel(models.Model):
    name = models.CharField(max_length=255, verbose_name='Nombre')
    email = models.EmailField(verbose_name='Email')
    phone_number = models.CharField(max_length=20, verbose_name='Numero de telefono')
    subject = models.CharField(max_length=255, verbose_name='Asunto')
    message = models.TextField(verbose_name='Mensaje')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'
        ordering = ['-created']

    def __str__(self):
        return self.name


class QuickContactModel(models.Model):
    name = models.CharField(max_length=255, verbose_name='Nombre')
    email = models.EmailField(verbose_name='Email')
    phone_number = models.CharField(max_length=20, verbose_name='Numero de telefono')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'Contacto Rápido'
        verbose_name_plural = 'Contactos Rápidos'
        ordering = ['-created']

    def __str__(self):
        return self.name

