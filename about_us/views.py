from django.shortcuts import render
from .models import MisionModel, VisionModel, ValuesModel, TeamModel, TestimonialModel, Client
from  solutions.models import PrincipalSolution
from django.views.generic import TemplateView, ListView


class AboutUsView(TemplateView):
    template_name = 'about_us/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['missions'] = MisionModel.objects.all()
        context['visions'] = VisionModel.objects.all()
        context['values'] = ValuesModel.objects.all()
        context['personal'] = TeamModel.objects.all()
        context['testimonials'] = TestimonialModel.objects.all()
        context['clients'] = Client.objects.all()
        context['solutions'] = PrincipalSolution.objects.all()
        return context


class TestimonialView(ListView):
    model = TestimonialModel
    template_name = 'about_us/testimonials.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['missions'] = MisionModel.objects.all()
        context['visions'] = VisionModel.objects.all()
        context['values'] = ValuesModel.objects.all()
        context['personal'] = TeamModel.objects.all()
        context['testimonials'] = TestimonialModel.objects.all()
        context['clients'] = Client.objects.all()
        context['solutions'] = PrincipalSolution.objects.all()
        return context


class TeamView(ListView):
    model = TeamModel
    template_name = 'about_us/team.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['missions'] = MisionModel.objects.all()
        context['visions'] = VisionModel.objects.all()
        context['values'] = ValuesModel.objects.all()
        context['personal'] = TeamModel.objects.all()
        context['testimonials'] = TestimonialModel.objects.all()
        context['clients'] = Client.objects.all()
        context['solutions'] = PrincipalSolution.objects.all()
        return context



