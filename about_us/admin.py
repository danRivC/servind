from django.contrib import admin
from .models import *


class ImageMisionInLine(admin.TabularInline):
    model = ImageMision


class ImageVisionInLine(admin.TabularInline):
    model = ImageVision


class ImageValuesInLine(admin.TabularInline):
    model = ImageValues


class MisionAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'updated']
    inlines = [
        ImageMisionInLine
    ]


class ValueAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'updated']
    inlines = [
        ImageValuesInLine
    ]


class VisionAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'updated']
    inlines = [
        ImageVisionInLine
    ]


class TestimonialAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'updated']



class ClientAdmin(admin.ModelAdmin):
    readonly_fields = ['created', 'updated']


admin.site.register(Client, ClientAdmin)
admin.site.register(TestimonialModel, TestimonialAdmin)
admin.site.register(TeamModel)
admin.site.register(MisionModel, MisionAdmin)
admin.site.register(ValuesModel, ValueAdmin)
admin.site.register(VisionModel, VisionAdmin)
