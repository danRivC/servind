from django.db import models

# Create your models here.
class MisionModel(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descripcion')

    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'misión'
        verbose_name_plural= 'misiones'
        ordering=['-created']

    def __str__(self):
        return self.title


class ImageMision(models.Model):
    mission = models.ForeignKey('about_us.MisionModel', on_delete=models.CASCADE, related_name='imagesMision')
    image = models.ImageField(upload_to='mision/images')

    def __str__(self):
        return self.image.url

class VisionModel(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descripcion')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'visión'
        verbose_name_plural= 'visiones'
        ordering=['-created']

    def __str__(self):
        return self.title


class ImageVision(models.Model):
    mission = models.ForeignKey('about_us.VisionModel', on_delete=models.CASCADE, related_name='imagesVision')
    image = models.ImageField(upload_to='vision/images')

    def __str__(self):
        return self.image.url


class ValuesModel(models.Model):
    title = models.CharField(max_length=255, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descripcion')

    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'valor'
        verbose_name_plural= 'valores'
        ordering=['-created']
    def __str__(self):
        return self.title


class ImageValues(models.Model):
    mission = models.ForeignKey('about_us.ValuesModel', on_delete=models.CASCADE, related_name='imagesValues')
    image = models.ImageField(upload_to='values/images')

    def __str__(self):
        return self.image.url


class TeamModel(models.Model):
    name_personal = models.CharField(max_length=255, verbose_name='Nombre')
    profession = models.CharField(max_length=255, verbose_name='Profesión')
    image = models.ImageField(upload_to='personal', verbose_name= 'Imagen de Perfil')
    twitter_link = models.URLField(verbose_name='Link de Twitter')
    linkedin_link = models.URLField(verbose_name='Link de Linkedin')
    facebook_link = models.URLField(verbose_name='Link de Facebook')
    skype_link = models.URLField(verbose_name='Link de Skype')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'personal'
        verbose_name_plural = 'personal'
        ordering=['-created']

    def __str__(self):
        return self.name_personal


class TestimonialModel(models.Model):
    corp_name = models.CharField(max_length=220, verbose_name= 'Nombre de la empresa')
    name = models.CharField(max_length=255, verbose_name='Nombre')
    content = models.TextField(verbose_name='Contenido')
    contact_url = models.URLField(verbose_name='Link de contacto')
    image_person = models.ImageField(upload_to='testimonial/', verbose_name='Imagen del testimonio')
    business_position = models.CharField(max_length=220, verbose_name='Cargo empresarial')
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'testimonio'
        verbose_name_plural = 'testimonios'
        ordering = ['-created']

    def __str__(self):
        return self.name


class Client(models.Model):
    name = models.CharField(max_length=255, verbose_name='nombre')
    image = models.ImageField(upload_to='clientes/', verbose_name='imagen')
    website = models.URLField(verbose_name='Página web', null=True, blank=True)
    created = models.DateField(auto_now_add=True, verbose_name='Fecha de creacion')
    updated = models.DateField(auto_now=True, verbose_name='Fecha de modificación')

    class Meta:
        verbose_name = 'cliente'
        verbose_name_plural = 'clientes'
        ordering = ['-created']

    def __str__(self):
        return self.name
