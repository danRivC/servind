from django.urls import path
from .views import *
urlpatterns=[
    path('', AboutUsView.as_view(), name='about'),
    path('testimonial/', TestimonialView.as_view(), name='testimonial'),
    path('our-team/', TeamView.as_view(), name='equipo'),
]